/// <reference types="cypress" />

describe('Home page', () => {

  beforeEach(() => {
    cy.visit(Cypress.env('indexUrl'))
  })
  it('has the name Bookmark as title', () => {
    cy.title().should('contain','Bookmark')
  })

  it('has a header section with a menu', () => {
    cy.get('.header')
    cy.get('.header').within(() => {
      cy.get('a').should('have.attr', 'class', 'header__logo')
      cy.get('.menu__item').should('have.length', 4 )
    })
  });

  it('has a hero section', () => {
    cy.get('.hero')
    cy.get('.hero').within(() => {
      cy.get('.hero__image')
      cy.get('.hero__title')
      cy.get('.hero__copy')
      cy.get('.hero__button').should('have.length', 2 )
      cy.get('.hero__button--cta').should('have.length', 1)
    })
  });

  it('has a features section ', () => {
    cy.get('.features')
    cy.get('.features').within(() => {
      cy.get('.features__title')
      cy.get('.features__copy')

      cy.get('.tabs').within(() => {
        cy.get('.tabs__link').should('have.length', 3 )

        cy.get('.tabs__content')
        cy.get('article.tab').within(() => {
          cy.get('.tab__image')
          cy.get('.tab__title')
          cy.get('.tab__copy')
          cy.get('.tab__link')
        })
      })
    })
  });

  it('has a download section', () => {
    cy.get('.download')
    cy.get('.download').within(() => {
      cy.get('.download__title')
      cy.get('.download__desc')
      cy.get('.download__item').should('have.length', 3 )
      cy.get('.download__item .item__image').should('have.length', 3 )
      cy.get('.download__item .item__title').should('have.length', 3 )
      cy.get('.download__item .item__desc').should('have.length', 3 )
      cy.get('.download__item .item__button').should('have.length', 3 )
    })
  });

  it('has a faq section', () => {
    cy.get('.faq')
    cy.get('.faq').within(() => {
      cy.get('.faq__title')
      cy.get('.faq__desc')
      cy.get('.accordion')
      cy.get('.accordion__item').should('have.length', 4 )

    })
    
  });

  it('has a join section', () => {
    cy.get('.join')
    cy.get('.join').within(() => {
      cy.get('.join__copy')
      cy.get('.join__title')
      cy.get('.join-form')
      cy.get('.join-form').within(() => {
        cy.get('.join-form__input')
        cy.get('.join-form__button')

      })    
    })
  });

  it('has a footer', () => {
    cy.get('.footer')
    cy.get('.footer').within(() => {
      cy.get('.footer-menu')
      cy.get('.footer-menu').within(() => {
        cy.get('.footer-menu__link').should('have.length', 3 )
      })
      cy.get('.footer-social')
      cy.get('.footer-social').within(() => {
        cy.get('.footer-social__fb')
        cy.get('.footer-social__tw')
      })
    })
  });
})